# Kronometrix IoT Gateway

## Overview

An automatic, simple to use, data collection and transport utility with a low system footprint, supporting raw data. It’s available for Industrial IoT, ICT 
enterprise, weather and environment.

* [Getting started](docs/start.md)
* [FAQ](docs/faq.md)

It is available to run as a ready made system image for Raspberry PI single board computers, including: the 64bit operating system, [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) module and all necessarily configurations.

Kronometrix IoT Gateway (Code name: K<sub>1</sub>) is a commercial product designed and supported by Kronometrix Analytics Finland. The product can be purchased as a MicroSD card for Raspberry PI computers or as a complete IoT gateway. The product comes with 1 year warranty and standard software support. For additional 24x7 support, unlimited phone and email tickets please contact sales@kronometrix.com

**Note**

Kronometrix IoT Gateway uses by default [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) an open-source project, offering a range of data recorders across many operating systems and platforms. You can separately download and use the data recording module for Linux, FreeBSD, Windows or MacOS 64bit. Check [FAQ](docs/faq.md) for more information.

## Supported Hardware

  * Raspberry PI 3B+, 4B

## Connectivity

  * RS232/485, USB, RJ45

## Protocols

  * TCP, UDP, HTTP, SNMP, MODBUS(RTU, TCP, ASCII), SERIAL ASCII-POLLED

For more information please visit: https://www.kronometrix.com/products/iotgateway/

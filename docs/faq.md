## FAQ

### What is the difference between Kronometrix Data Recording and IoT Gateway?
[Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) is an open-source project, available to be used in various other projects and products. 
Anyone can take the data recording project and use it as defined in the [LICENSE](https://gitlab.com/kronometrix/recording/-/blob/master/LICENSE) distribution. 

[Kronometrix IoT Gateway](https://www.kronometrix.com/products/iotgateway/) is a commercial product (Internal code name: K<sub>1</sub>), developed by [Kronometrix Analytics](https://www.kronometrix.com) Finland, using [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) and other software applications and configuration files for ARM64 architecture. At this moment, ARM single-board computers, Raspberry PI 3B, 3B+, 4B are supported. Additional, several commercial support services are included for Kronometrix IoT Gateway product. No commercial support is offered for [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording). 

### I want to use webrec, svcrec data recorders. Do I really need Kronometrix IoT Gateway?
No, you dont. You can easily install the data recording package for your operating system, configure and run webrec and svcrec data recorders. If you need support you can join, 

### We want to measure the performance of 10 web sites and some email services. How can we do that ? 
You can get Kronometrix IoT Gateway. Then you will need to configure web and services data recorders for your configurations. Or you can download [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) and run it for your project, configuring all needed components: data recorders and transport utility.

### We are a solution provider. We want to record data from weather and air quality sensors. Can we use Kronometrix IoT Gateway?
Absolutely. You can easily use Kronometrix IoT Gateway to connect your sensors and devices and record data from. Additional, Kronometrix IoT Gateway can be embeded to your own solution, using a single-board computer, like Raspberry PI or other ARM based SBCs. Kronometrix IoT Gateway is simple to integrate and embed to your projects.

### We want to measure our ICT enterprise services inside and outside our network. How can we use Kronometrix IoT Gateway?
The simplest method is to use our [CloudView](https://www.kronometrix.com/cloudview/) service, which offers integrated capabilities to measure web and Internet services inside and outside of your own network. Another way, is that you build your own measuring network, inside and outside your business, using Kronometrix IoT Gateway or [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording.

### Can I download Kronometrix Data Recording and build my own product?
Absolutely. [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) is an open-source project allowing developers, system integrators, devops to 
build new recorders, or totally new products around it. For more information about usage and distribution please check the 
[LICENSE](https://gitlab.com/kronometrix/recording/blob/master/LICENSE).

### What does Kronometrix IoT Gateway image contain?
The gateway system image contains, the operating system, [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording), data recorders, configurations, the HTTP transporter. Additional, Kronometrix IoT Gateway system images have been tuned and optimised for security and performance up to 100 data-sources.

### Im not convinced. Why should I use Kronometrix IoT Gateway?
Kronometrix IoT Gateway is a complete and integrated software solution for ARM single-board computers, which contains all needed modules to record and transport data, offering at the same time access to raw data and management functions. It is aimed for industrial IoT, edge or fog computing, to access different hardware equipment and sensors. If you work and develop various solutions in these industries, Kronometrix IoT Gateway is simple and efficient to use, coming with a commercial support 8x5 or 24x7. For example Kronometrix IoT Gateway is used as the default IoT gateway for Kronometrix Indoor Air Quality, Weather and Industrial IoT measurements.

### What hardware architecture Kronometrix IoT Gateway is supporting?
ARM v8 (64bit), single-board computers, Raspberry PI 3B, 3B+, 4B.

### Can I add new data recorders?
Yes, you can. Please visit and join, [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) project. Open a new issue for data recording module. Or join our [discussion forum](https://discussions.kronometrix.com)

### Can I change an existing data recorder, to improve it?
Yes, you can. Please visit and join, [Kronometrix Data Recording](https://gitlab.com/kronometrix/recording) project. Open a new issue for particular data recorder you are interested in and we can work together. We are more than happy to receive help.

### Does Kronometrix IoT Gateway run on embedded systems or micro-controller architectures?
No. If you are interested in porting Kronometrix IoT Gateway to a micro-controller architecture please contact us support@kronometrix.com or join our [discussion forum](https://discussions.kronometrix.com)  

### Why Kronometrix IoT Gateway is using a general purpose UNIX based operating system?
Because simplicity, performance, management and costs. To build an IoT gateway requires access to storage, network and serial communication protocols which 
general purpose UNIX/POSIX based operating systems are providing. Embedded systems and micro-controller architectures have a reduced number of functions, 
with a more complex development and support path.

### Why Kronometrix IoT Gateway is not using Raspbian operating system ?
Kronometrix technologies have selected FreeBSD as the operating system for the data analytics platform, gateway, databus products based on its merits:
performance, scalability and security. We have conducted a number of internal tests where we have compared Debian/Raspbian and FreeBSD. We have found that 
FreeBSD was performing better than the Linux based variants in: storage IO, network, security and management. 

### Why Kronometrix IoT Gateway is not running as a Windows based gateway?
Because of the licensing model, and technology stack. Windows is not a POSIX based operating system.

# Getting started

## Kronometrix IoT Gateway
An automatic, simple to use, data collection and transport utility with a small system footprint, supporting raw data, available for Industrial IoT, ICT enterprise, weather and environment. Please visit [Kronometrix IoT Gateway](https://www.kronometrix.com/products/iotgateway/) for more information about the product specifications and price.

![K<sub>1</sub> MicroSD](/docs/img/K1.MicroSD.300.png)
![K<sub>1</sub> IoT Gateway](/docs/img/K1.IoTGateway.png)

## Kronometrix Data Recording
For other type of installations and deployments, where you do not require the complete Kronometrix IoT Gateway product, you can download, configure and run the data recording module itself. No support is offered by default. However you can join discussions.kronometrix.com community and ask anything you like about Kronometrix data recorders or the IoT gateway.

### Linux

Please check [README.linux](https://gitlab.com/kronometrix/recording/blob/master/README.linux) to verify the system prerequisites, how to install and setup KDR on Linux like operating systems.

#### RPM based systems

##### Download package
* x64: [kdr-stable-rhel-x64.rpm](https://gitlab.com/kronometrix/k1/-/raw/master/linux/kdr-stable-rhel-x64.rpm)

##### Install package 
* ```# rpm -ihv kdr-stable-rhel-x64.rpm```
  
#### DEB based systems

##### Download packages
* x64: [kdr-stable-debian-x64.deb](https://gitlab.com/kronometrix/k1/-/raw/master/linux/kdr-stable-debian-x64.deb)
* armv8: [kdr-stable-raspbian-armv8.deb](https://gitlab.com/kronometrix/k1/-/raw/master/linux/kdr-stable-raspbian-armv8.deb) 
    
##### Install package 
* ```# dpkg -i kdr-stable-debian-x64.deb``` 

### UNIX

Please check [README.freebsd](https://gitlab.com/kronometrix/recording/blob/master/README.freebsd)
to verify the system prerequisites, how to install and setup KDR on FreeBSD UNIX operating systems.

#### FreeBSD systems

##### Download packages
* x64: [kdr-stable-freebsd-x64.txz](https://gitlab.com/kronometrix/k1/-/raw/master/freebsd/kdr-stable-freebsd-x64.txz)
* armv8: [kdr-stable-freebsd-arm.txz](https://gitlab.com/kronometrix/k1/-/raw/master/freebsd/kdr-stable-freebsd-arm.txz) 
    
##### Install package 
* ```# pkg install kdr-stable-freebsd-x64.txz``` 

### Windows

Please check [README.windows](https://gitlab.com/kronometrix/recording/blob/master/README.windows)
to verify the system prerequisites, how to install and setup KDR on Windows operating systems.

#### Windows 2008, 2012, 2016 systems

##### Download package
* x64: [kdr-stable-windows-x64.exe](https://gitlab.com/kronometrix/k1/-/raw/master/win/kdr-stable-windows-x64.exe)

##### Install package 
* Execute ```kdr-stable-windows-x64.exe```
